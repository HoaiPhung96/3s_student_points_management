﻿using System;
using System.Collections.Generic;

namespace QuanLyDiemSV
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> listStudents = new List<Student>();
            int n;
            Console.Write("Nhap so SV: ");

            do
            {
                n = Convert.ToInt32(Console.ReadLine());
                if (n <= 0)
                {
                    Console.WriteLine("Vui long nhap 1 sv");
                    Console.Write("Nhap lai so sv: ");
                }
                else break;
            } while (n > 0);

            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("Nhap diem sv thu {0}", (i + 1));
                Student sd = new Student();
                sd.Nhap();
                listStudents.Add(sd);
                Console.WriteLine("------------------------");
            }

            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("Diem sv thu {0}", (i + 1));
                listStudents[i].Xuat();
                Console.WriteLine("-------------------------");
            }

            Student temp;
            for (int i = 0; i < n; i++)
            {
                for (int j = i+1; j < n; j++)
                {
                    if (listStudents[j].DiemTB(listStudents[j].DiemToan1, listStudents[j].DiemLy1,
                            listStudents[j].DiemHoa1) > listStudents[i].DiemTB(listStudents[i].DiemToan1,listStudents[i].DiemLy1,listStudents[i].DiemHoa1))
                    {
                        temp = listStudents[i];
                        listStudents[i] = listStudents[j];
                        listStudents[j] = temp;
                    }
                }
            }
            Console.WriteLine("Danh sach 3 sinh vien giam dan theo diem trung binh ");
            for (int i = 0; i <= 2; i++)
            {
                Console.WriteLine("Ho ten {0}, Diem Trung binh {1}", listStudents[i].TenSv,
                    listStudents[i].DiemTB(listStudents[i].DiemToan1, listStudents[i].DiemLy1,
                        listStudents[i].DiemHoa1));
            }

            Console.ReadKey();
        }
    }
}