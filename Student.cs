﻿using System;

namespace QuanLyDiemSV
{
    public class Student
    {
        private string TenSV;
        private int DiemToan;
        private int DiemLy;
        private int DiemHoa;

        public string TenSv
        {
            get => TenSV;
            set => TenSV = value;
        }

        public int DiemToan1
        {
            get => DiemToan;
            set => DiemToan = value;
        }

        public int DiemLy1
        {
            get => DiemLy;
            set => DiemLy = value;
        }

        public int DiemHoa1
        {
            get => DiemHoa;
            set => DiemHoa = value;
        }


        public void Nhap()
        {
            Console.Write("Nhap Ten: ");
            TenSV = Console.ReadLine();
            
            
            Console.Write("Nhap diem toan: ");
            do
            {
                DiemToan = Convert.ToInt32(Console.ReadLine());
                if (DiemToan > 10 || DiemToan < 0)
                {
                    Console.WriteLine("Diem phai nam trong khoan 0-10");
                    Console.Write("Nhap lai diem toan");
                }
                else break;
            } while (DiemToan < 10|| DiemToan > 0 );
            
          
            
            Console.Write("Nhap diem ly: ");
            do
            {
                DiemLy = Convert.ToInt32(Console.ReadLine());
                if (DiemLy > 10 || DiemLy < 0)
                {
                    Console.WriteLine("Diem phai nam trong khoan 0-10");
                    Console.Write("Nhap lai diem ly");
                }
                else break;
            } while (DiemLy < 10|| DiemLy > 0 );
            
            
            
            
            Console.Write("Nhap diem toan: ");
            do
            {
                DiemHoa = Convert.ToInt32(Console.ReadLine());
                if (DiemHoa > 10 || DiemHoa < 0)
                {
                    Console.WriteLine("Diem phai nam trong khoan 0-10");
                    Console.Write("Nhap lai diem hoa");
                }
                else break;
            } while (DiemHoa < 10|| DiemHoa > 0 );
        }


        public float DiemTB(float DiemToan, float DiemLy, float DiemHoa)
        {
            return (DiemToan + DiemLy + DiemHoa) / 3;
        }
        
        public void Xuat()
        {
            Console.WriteLine("TenSV: {0}|| DiemToan: {1} || DiemLy: {2}|| DiemHoa: {3}|| DiemTB: {4}", TenSV,DiemToan,DiemLy,DiemHoa,DiemTB(DiemToan,DiemLy,DiemHoa));
        }
    }
    
    
}